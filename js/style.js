document.addEventListener("DOMContentLoaded", function() {
  let tabs = document.querySelectorAll(".tabs-title");
  let tabContents = document.querySelectorAll(".tabs-content li");

  function switchTab(tabIndex) {
    tabContents.forEach(function(content) {
      content.style.display = "none";
    });

    tabs.forEach(function(tab) {
      tab.classList.remove("active");
    });

    tabs[tabIndex].classList.add("active");
    tabContents[tabIndex].style.display = "block";
  }

  tabs.forEach(function(tab, index) {
    tab.addEventListener("click", function() {
      switchTab(index);
    });
  });

  switchTab(0);
});